^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package custom_dbw_sim_examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.1.0 (2024-02-26)
------------------
* Implemented DBW2 interface and added backward compatible switch to DBW1
* Contributors: Micho Radovnikovich

3.0.1 (2022-11-17)
------------------

3.0.0 (2022-09-08)
------------------
* First ROS 2 release
* Contributors: Micho Radovnikovich

2.6.0 (2021-12-07)
------------------
* Removed concept of car year; Moved example sensors to example package
* Separated example simulation configs into a separate package for open-source
* Contributors: Micho Radovnikovich

2.5.1 (2020-11-18 17:23)
------------------------

2.5.0 (2020-08-05 12:02)
------------------------

2.4.1 (2020-11-18 17:20)
------------------------

2.4.0 (2020-08-05 10:29)
------------------------

2.3.0 (2019-11-11)
------------------

2.2.0 (2019-10-17)
------------------

2.1.2 (2019-08-09)
------------------

2.1.1 (2019-07-06)
------------------

2.1.0 (2019-06-25)
------------------

2.0.2 (2019-05-03)
------------------

2.0.1 (2019-04-23)
------------------

2.0.0 (2019-03-15)
------------------
