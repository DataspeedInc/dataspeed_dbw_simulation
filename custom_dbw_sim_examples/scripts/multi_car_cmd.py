#! /usr/bin/env python3
import rclpy
from rclpy.node import Node
import ds_dbw_msgs.msg
import dataspeed_ulc_msgs.msg
from std_msgs.msg import Empty

class MultiCarCmd(Node):
    def __init__(self) -> None:
        super().__init__('multi_car_cmd')

        self.dbw2 = self.declare_parameter('dbw2', True).value
        if self.dbw2:
            self.pub_mach_e_ulc_cmd = self.create_publisher(ds_dbw_msgs.msg.UlcCmd, '/mach_e/ulc/cmd', 1)
            self.pub_mach_e_steer_cmd = self.create_publisher(ds_dbw_msgs.msg.SteeringCmd, '/mach_e/steering/cmd', 1)
            self.pub_pacifica_ulc_cmd = self.create_publisher(ds_dbw_msgs.msg.UlcCmd, '/pacifica/ulc/cmd', 1)
            self.pub_pacifica_steer_cmd = self.create_publisher(ds_dbw_msgs.msg.SteeringCmd, '/pacifica/steering/cmd', 1)
        else:
            self.pub_mach_e_ulc_cmd = self.create_publisher(dataspeed_ulc_msgs.msg.UlcCmd, '/mach_e/ulc_cmd', 1)
            self.pub_pacifica_ulc_cmd = self.create_publisher(dataspeed_ulc_msgs.msg.UlcCmd, '/pacifica/ulc_cmd', 1)

        self.pub_mach_e_enable = self.create_publisher(Empty, '/mach_e/enable', 1)
        self.pub_pacifica_enable = self.create_publisher(Empty, '/pacifica/enable', 1)
        self.create_timer(0.02, self.timer_callback)

    def timer_callback(self):
        if self.dbw2:
            ulc_cmd = ds_dbw_msgs.msg.UlcCmd()
            ulc_cmd.cmd_type = ds_dbw_msgs.msg.UlcCmd.CMD_VELOCITY
            ulc_cmd.enable = True
            ulc_cmd.enable_shift = True
            ulc_cmd.enable_shift_park = True
            ulc_cmd.cmd = 2.0

            steer_cmd = ds_dbw_msgs.msg.SteeringCmd()
            steer_cmd.enable = True
            steer_cmd.cmd_type = ds_dbw_msgs.msg.SteeringCmd.CMD_YAW_RATE
            steer_cmd.cmd = 0.25
            self.pub_mach_e_ulc_cmd.publish(ulc_cmd)
            self.pub_mach_e_steer_cmd.publish(steer_cmd)

            steer_cmd.cmd = -0.25
            self.pub_pacifica_ulc_cmd.publish(ulc_cmd)
            self.pub_pacifica_steer_cmd.publish(steer_cmd)
        else:
            ulc_cmd = dataspeed_ulc_msgs.msg.UlcCmd()
            ulc_cmd.enable_pedals = True
            ulc_cmd.enable_shifting = True
            ulc_cmd.shift_from_park = True
            ulc_cmd.pedals_mode = dataspeed_ulc_msgs.msg.UlcCmd.SPEED_MODE
            ulc_cmd.linear_velocity = 2.0

            ulc_cmd.enable_steering = True
            ulc_cmd.steering_mode = dataspeed_ulc_msgs.msg.UlcCmd.YAW_RATE_MODE
            ulc_cmd.yaw_command = 0.25
            self.pub_mach_e_ulc_cmd.publish(ulc_cmd)

            ulc_cmd.yaw_command = -0.25
            self.pub_pacifica_ulc_cmd.publish(ulc_cmd)

        self.pub_pacifica_enable.publish(Empty())
        self.pub_mach_e_enable.publish(Empty())


def main(args=None):
    rclpy.init(args=args)
    node_instance = MultiCarCmd()
    rclpy.spin(node_instance)

if __name__ == '__main__':
    main()
